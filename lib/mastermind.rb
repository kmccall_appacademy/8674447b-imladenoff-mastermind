class Code
  attr_reader :pegs

  PEGS = {
    r: "red",
    o: "orange",
    y: "yellow",
    g: "green",
    b: "blue",
    p: "purple"
  }

  def initialize pegs
    @pegs = pegs
  end

  def []i
    pegs[i]
  end

  def ==other_code
    return false if !other_code.is_a?(Code)
    self.pegs == other_code.pegs
  end

  def self.random
    pegs = []
    (0..3).each { |i| pegs[i] = PEGS.values.sample }
    Code.new pegs
  end

  def self.parse input
    pegs = input.split("").map do |peg|
      raise "That's not a color you can choose." if !PEGS.has_key? peg.downcase.to_sym
      peg.downcase.to_sym
    end

    Code.new pegs
  end

  def exact_matches other_code
    counter = 0

    pegs.each.with_index do |peg,i|
      counter += 1 if peg == other_code[i]
    end

    counter
  end

  def near_matches other_code
    counter = 0

    # pegs.each.with_index do |peg,i|
    #   counter += 1 if (peg != other_code[i]) && other_code.pegs.include?(peg)
    # end
    #
    self.color_totals

    other_code.color_totals.each do |color, total|
      case total <=> self.color_totals[color]
      when -1
        counter += total
      when 0
        counter += total
      when 1
        counter += self.color_totals[color]
      end
    end

    counter - self.exact_matches(other_code)
  end

  def color_totals
    totals = Hash.new 0

    PEGS.each { |k,v| totals[k] = 0}

    pegs.each do |peg|
      totals[peg] += 1
    end
    totals
  end

end

class Game
  attr_reader :secret_code

  TURNS = 10

  def initialize secret_code=Code.random
    @secret_code = secret_code
  end

  def get_guess
    puts "Input your guess!"
    guess = gets.chomp
    Code.new guess
  end

  def display_matches guess
    spot_on = secret_code.exact_matches guess
    near = secret_code.near_matches guess
    puts "You got #{spot_on} exact match and #{near} near matches!"
  end
end
